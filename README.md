# README #
Java EE e-commerce (dummy) implementation with customizable products, shopping cart, and checkout functionality.

I used: Java EE 7 Web, Apache Tomcat 8 server, MySQL database, and Netbeans.

I interacted with the DB by using the Java Persistence API (JPA), an object-relational mapping tool.