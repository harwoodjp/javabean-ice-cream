/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package icecream_data;

import icecream_objects.Cart;
import icecream_objects.Transaction;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

/**
 *
 * @author j
 */
public class TransactionDB {
    
    public static List<Transaction> selectTransactions() {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String qString = "SELECT p from Transaction p ";
        TypedQuery<Transaction> q = em.createQuery(qString, Transaction.class);
        
        List<Transaction> transactions;
        try {
            transactions = q.getResultList();
            if (transactions == null || transactions.isEmpty())
                transactions = null;
        } finally {
            em.close();
        }
        return transactions;
    }
    
    public static void insert(Transaction entry) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(entry);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
        
    }
    
    
    
}
