/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package icecream_data;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author j
 */
public class DBUtil {
    
    private static final EntityManagerFactory emf =Persistence.createEntityManagerFactory("IceCreamPU");
    
    public static final EntityManagerFactory getEmFactory() {
        return emf;
    }

}
