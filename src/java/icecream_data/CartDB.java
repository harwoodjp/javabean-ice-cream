/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package icecream_data;

import icecream_objects.Cart;
import icecream_objects.Product;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

/**
 *
 * @author j
 */
public class CartDB {

    
    
    public static List<Cart> selectCart() {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String qString = "SELECT p from Cart p ";
        TypedQuery<Cart> q = em.createQuery(qString, Cart.class);
        
        List<Cart> cart;
        try {
            cart = q.getResultList();
            if (cart == null || cart.isEmpty())
                cart = null;
        } finally {
            em.close();
        }
        return cart;
    }
    
    public static void insert(Cart item) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(item);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
        
    }
    
    public static void clear_cart() {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String qString = "DELETE from Cart";
        TypedQuery<Cart> q = em.createQuery(qString, Cart.class);
        q.executeUpdate();
        
    }
    
    
    public static void delete(Cart item) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.remove(em.merge(item));
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
        
    }
    
    
}
