/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import icecream_data.CartDB;
import icecream_objects.Cart;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author j
 */
@WebServlet(urlPatterns = {"/cart_checkout"})
public class cart_checkout extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        List<Cart> cart = CartDB.selectCart();            

        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<style>\n" +
                    ".box_around_text {\n" +
                    "    width: 300px;\n" +
                    "    padding: 10px;\n" +
                    "    border: 1px solid black;\n" +
                    "    margin: 25px;\n" +
                    "}\n");
            out.println("h3,p,div,a,td {\n" +
"	font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;\n" +
"	font-style: normal;\n" +
"	font-variant: normal;\n" +
"	font-weight: 500;\n" +
"	line-height: 26.4px;\n" +
"}");
            out.println("</style>");
            
            
            
            out.println("</head>");
                        
            out.println("<body>");
            
            out.println("<center>");            
            out.println("<h3>🍦 Welcome to JavaBean Ice Cream 🍦</h3>");
            out.println("<p>Here's your cart...</p>");
            
                       
            out.println("<table style='border:1px solid black;     border-collapse: collapse;'>");
            out.println("<tr>");
            out.println("<th style='border:1px solid black;     border-collapse: collapse;'>Item</th>");
            out.println("<th style='border:1px solid black;     border-collapse: collapse;'>Price</th>");
            out.println("</tr>"); 
            
            double sum_price = 0.0;
            
            for (int i = 0; i < cart.size(); i++) {
                sum_price = sum_price + cart.get(i).price;
                out.println("<tr style='border:1px solid black;     border-collapse: collapse;'>");                
                out.println("<td style='border:1px solid black;     border-collapse: collapse;'>");
                out.println(cart.get(i).description);
                out.println("</td>");

                out.println("<td style='border:1px solid black;     border-collapse: collapse;'>");
                out.println(cart.get(i).price);
                out.println("</td>");
                out.println("</tr>");            
            }
            out.println("</table>");
            
            out.println("<br><p>That'll be $" + sum_price + " total.</p>");      
            
            
            out.println("<table>");          
            out.println("<form action='confirm_checkout'>");
    
            out.println("<tr>");
            out.println("<td>Name:</td>  <td><input type='text' name='name'></td>");
            out.println("</tr>");
            
            out.println("<tr>");            
            out.println("<td>Card Type:</td> <td><input type='text' name='card_type'></td>");            
            out.println("</tr>");
            
            out.println("<tr>");                        
            out.println("<td>Card #:</td>  <td><input type='text' name='card_num'></td>");
            out.println("</tr>");    
            
            out.println("<tr>");                        
            out.println("<td>Discount:</td>  <td><input type='text' name='discount'></td>");
            out.println("</tr>");            
            
            
            out.println("</table>");
            
            out.println("<br><input type='submit' value='Submit' />");                              
            out.println("</form>");
            
            
            
            out.println("<p><a href='/IceCream/test_servlet'> Home</a> </p>");            
            
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
