/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import icecream_data.TransactionDB;
import icecream_objects.Transaction;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author j
 */
@WebServlet(urlPatterns = {"/transaction_servlet"})
public class transaction_servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        List<Transaction> transactions = TransactionDB.selectTransactions();            
        
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<style>\n" +
                    ".box_around_text {\n" +
                    "    width: 300px;\n" +
                    "    padding: 10px;\n" +
                    "    border: 1px solid black;\n" +
                    "    margin: 25px;\n" +
                    "}\n");
            out.println("h3,p,div,a,td {\n" +
"	font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;\n" +
"	font-style: normal;\n" +
"	font-variant: normal;\n" +
"	font-weight: 500;\n" +
"	line-height: 26.4px;\n" +
"}");
                        
            out.println("</style>");
            
            
            
            
            out.println("</head>");
                        
            out.println("<body>");
            
            out.println("<center>");            
            out.println("<h3>🍦 Welcome to JavaBean Ice Cream 🍦</h3>");
            out.println("<p>All transactions:</p>");
            
                       
            out.println("<table style='border:1px solid black;     border-collapse: collapse;'>");
            out.println("<tr>");
            out.println("<th style='border:1px solid black;     border-collapse: collapse;'>Order</th>");
            out.println("<th style='border:1px solid black;     border-collapse: collapse;'>Name</th>");
            out.println("<th style='border:1px solid black;     border-collapse: collapse;'>Card Type</th>");
            out.println("<th style='border:1px solid black;     border-collapse: collapse;'>Card Number</th>");
            out.println("<th style='border:1px solid black;     border-collapse: collapse;'>Price</th>");
            out.println("</tr>"); 
            
            
            for (int i = 0; i < transactions.size(); i++) {
                out.println("<tr style='border:1px solid black;     border-collapse: collapse;'>");                
                
                out.println("<td style='border:1px solid black;     border-collapse: collapse;'>");
                out.println(transactions.get(i).description);
                out.println("</td>");
                
                out.println("<td style='border:1px solid black;     border-collapse: collapse;'>");
                out.println(transactions.get(i).name);
                out.println("</td>");
                
                out.println("<td style='border:1px solid black;     border-collapse: collapse;'>");
                out.println(transactions.get(i).card_type);
                out.println("</td>");

                out.println("<td style='border:1px solid black;     border-collapse: collapse;'>");
                out.println(transactions.get(i).card_number);
                out.println("</td>");

                out.println("<td style='border:1px solid black;     border-collapse: collapse;'>");
                out.println(transactions.get(i).price);
                out.println("</td>");
                out.println("</tr>");            
            }
            out.println("</table>");
            
            out.println("<p><a href='/IceCream/test_servlet'> Home</a> </p>");            
            
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
