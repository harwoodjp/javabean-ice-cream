/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import icecream_data.CartDB;
import icecream_data.TransactionDB;
import icecream_objects.Cart;
import icecream_objects.Transaction;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author j
 */
@WebServlet(urlPatterns = {"/test_servlet"})
public class test_servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");        
                            
        //List<Cart> cart = CartDB.selectCart();            
        int cart_size;
        int transactions_size;
        
        if (CartDB.selectCart()==null || CartDB.selectCart().isEmpty()) {
            cart_size = 0;
        }
        else {
            List<Cart> cart = CartDB.selectCart();
            cart_size = cart.size();
        }
        if (TransactionDB.selectTransactions()==null || TransactionDB.selectTransactions().isEmpty()) {
            transactions_size = 0;
        }
        else {
            List<Transaction> transactions = TransactionDB.selectTransactions();
            transactions_size = transactions.size();
        }
        
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            
            out.println("<style>\n" +
                    ".box_around_text {\n" +
                    "    width: 300px;\n" +
                    "    padding: 10px;\n" +
                    "    border: 1px solid black;\n" +
                    "    margin: 25px;\n" +
                    "}\n");
            out.println("h3,p,div,a {\n" +
"	font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;\n" +
"	font-style: normal;\n" +
"	font-variant: normal;\n" +
"	font-weight: 500;\n" +
"	line-height: 26.4px;\n" +
"}");
            out.println("</style>");
            
            
                  
            out.println("</head>");
            out.println("<body>");
            
            out.println("<center>");            
            out.println("<h3>🍦 Welcome to JavaBean Ice Cream 🍦</h3>");
            out.println("<p>Please make a selection...</p>");
            
            
            out.println("<a href='cone_cup_select'><div class='box_around_text'>Cone/cup</div></a>");
            
            out.println("<a href='milkshake_select'><div class='box_around_text'>Milkshake</div></a>");

            out.println("<a href='float_select'><div class='box_around_text'>Float</div></a>");
            
           // out.println ("<a href='cart_servlet'> View Cart </a>");
            
            out.println ("<p><a href='cart_servlet'>View Cart (" + cart_size + ")</a> <a href='transaction_servlet'>View Transactions (" + transactions_size + ") </a></p>");


            out.println("</center>");            
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
