/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author j
 */
@WebServlet(urlPatterns = {"/cone_complete"})
public class cone_complete extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String cone = request.getParameter("cone");
        String scoops = request.getParameter("scoops");
        String flavor = request.getParameter("flavor");
        
        String cone_readable = cone;
        if (cone.contains("cup") == false) {
            cone_readable = cone + " cone";
        }

        String order_readable = "A " + cone_readable + " with " + scoops + " scoop(s) of " + flavor + ".";
        
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            
            out.println("<style>\n" +
                    ".box_around_text {\n" +
                    "    width: 300px;\n" +
                    "    padding: 10px;\n" +
                    "    border: 1px solid black;\n" +
                    "    margin: 25px;\n" +
                    "}\n");
            out.println("h3,p,div,a {\n" +
"	font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;\n" +
"	font-style: normal;\n" +
"	font-variant: normal;\n" +
"	font-weight: 500;\n" +
"	line-height: 26.4px;\n" +
"}");
                        
            out.println("</style>");
            
            
            out.println("</head>");
            out.println("<body>");
            
            
            out.println("<center>");            
            out.println("<h3>🍦 Welcome to JavaBean Ice Cream 🍦</h3>");
            out.println("<p>Sounds good. Let me make sure I got your cone/cup right. You wanted... </p>");
            
            out.println("<h3>" + order_readable + "</h3>");
            
            if (cone.contains("Waffle")) {
                out.println("<img src='http://www.snowballcone.com/images/P/large-waffle-707L%5B1%5D.gif'/>");
            }
            if (cone.contains("Cake")) {
                out.println("<img src='http://www.noveltycone.com/Images/gallery/novelty-25.gif'/>");
                   
            }
            if (cone.contains("Sugar")) {
                out.println("<img src='http://p-fst1.pixstatic.com/5069baedfb04d60a5100029f._w.1500_s.fit_.jpg'/>");
            }
            if (cone.contains("Cup")) {
                out.println("<img src='http://i.imgur.com/M2HnwOc.jpg'/>");
            }
            
            
            out.println("<div class='box_around_text'><a href='/IceCream/add_cone_to_cart?description=" + order_readable + "&scoops=" + scoops +"'> Yes, add this item to my cart. </a></div>");
            out.println("<div class='box_around_text'><a href='test_servlet'> No, start over. </a></div>");
            
            out.println("</center>");
            
            
            
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
