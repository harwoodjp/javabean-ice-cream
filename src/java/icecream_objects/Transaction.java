/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package icecream_objects;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author j
 */
@Entity
public class Transaction implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)    
    public int transactionId;
    public String description;
    public String name;
    public String card_type;
    public String card_number;
    public double price;
    
    
    public Transaction() {};
    
    public void setId(int transactionId) {
        this.transactionId = transactionId;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getCard_Type() {
        return card_type;
    }
    
    public void setDescription (String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setCard_Number (String card_number) {
        this.card_number = card_number;
    }
    
    public String getCard_Number() {
        return card_number;
    }


    public void setPrice(double price) {
        this.price = price;
    }
    public double getPrice() {
        return price;
    }    
    
    
}
